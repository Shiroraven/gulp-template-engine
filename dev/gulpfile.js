'use strict'

const gulp = require('gulp')
const postcss = require('gulp-postcss')
const sass = require('gulp-sass')
const pug = require('gulp-pug')
const copy = require('gulp-copy')
sass.compiler = require('node-sass')
const uncss = require('postcss-uncss')

// Compile Sass -----------------------------------------
const sassSelector = './sass/*.scss'
gulp.task('compile-sass', () => {
  let preprocessors = [uncss({html: './*.html'})]
  return gulp.src(sassSelector)
  .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(postcss(preprocessors))
  .pipe(gulp.dest('./css'))
})
// End Sass Compile -------------------------------------
// Compile Markup ---------------------------------------
const pugSelector = './template/*.pug'
gulp.task('compile-markup', () => {
  return gulp.src(pugSelector)
   .pipe(pug({pretty: true, filename: '.html'}))
   .pipe(gulp.dest('./'))
})
// End Markup Compile -----------------------------------
// Gulp Watch -------------------------------------------
const sassWatch = './sass/**/*.scss'
const pugWatch = './template/*'
gulp.task('watch', () => {
  gulp.watch(sassWatch, gulp.series('compile-sass'))
  gulp.watch(pugWatch, gulp.series('compile-markup'))
})
gulp.task('watch:sass', () => {
  gulp.watch(sassWatch, gulp.series('compile-sass'))
})
gulp.task('watch:markup', () => {
  gulp.watch(pugWatch, gulp.series('compile-markup'))
})
// End Gulp Watch ---------------------------------------
// Gulp Build -------------------------------------------
gulp.task('build:sass', () => {
  let preprocessors = [uncss({html: './*.html'})]
  return gulp.src(sassSelector)
  .pipe(sass.sync({outputStyle: 'compressed'}).on('error', sass.logError))
  .pipe(postcss(preprocessors))
  .pipe(gulp.dest('../build/css'))
})
gulp.task('build:markup', () => {
  return gulp.src(pugSelector)
  .pipe(pug({pretty: true, filename: '.html'}))
  .pipe(gulp.dest('../build'))
})
gulp.task('build:static', (done) => {
  gulp.src('./js/*')
   .pipe(copy('../build'))
   .pipe(gulp.dest('../build'))
  gulp.src('./assets/*')
   .pipe(copy('../build/assets'))
   .pipe(gulp.dest('../build/assets'))
   done()
})
gulp.task('build', gulp.series('build:markup', 'build:sass', 'build:static'))
// End Gulp Build ---------------------------------------